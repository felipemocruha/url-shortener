(ns url-shortener.core
  (:require [url-shortener.encoding :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen])
  (:gen-class))

(defn -main
  [& args]
  (println (gen/sample (s/gen :encoding/string) 10))
  (println (gen/sample (s/gen :encoding/number) 10)))

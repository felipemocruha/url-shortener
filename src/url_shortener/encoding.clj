(ns url-shortener.encoding
  (:require [clojure.spec.alpha :as s])
  (:gen-class))

(def alphabet "23456789abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ")
(def base (count alphabet))

(defn alphabet-valid? [s]
  (not (some nil? (map (fn [v] (some #{v} alphabet)) s))))

(s/def :encoding/number (s/and integer? #(> % 0)))
(s/def :encoding/string (s/and string?
                               alphabet-valid?
                               #(not (empty? %))))

(defn- concat-base [s n b]
  (str (get alphabet (mod n b)) s))

(defn encode [n]
  (loop [s "" n n]
    (if (> n 0)
      (recur (concat-base s n base) (biginteger (/ n base)))
      s)))

(defn decode [s]
  (loop [s s n 0 idx 0]
    (if (< idx (count s))
      (let [aidx (->> idx (get s) str (.indexOf alphabet))]
        (recur s (+ (* n base) aidx) (inc idx)))
      n)))
